from datetime import datetime
from my_class.ReceiptLine import ReceiptLine
class Receipt:
    n_Receipt = 1
    def __init__(self, lines: list):
        self.data = datetime.now()
        self.lines = lines
        self.num = Receipt.n_Receipt
        Receipt.n_Receipt += 1

    def add_line(self, line: ReceiptLine):
        self.lines.append(line)


