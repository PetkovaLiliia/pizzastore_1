from constants.pizza_item import pizza_item
from my_class.Pizza import Pizza
from my_class.Person import Person
from constants.cashiers import cashiers
import random
import itertools

def decor(method):
    def dec(s, x):
        print('*' * 64)
        method(s, x)
        print('-'*64, f"Numbers of orders: {len(x)}", sep='\n')
        print('*' * 64)
    return dec

class SingletonMeta(type):

    _instances = {}

    def __call__(cls, *args, **kwargs):

        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]

class PizzaStore(metaclass=SingletonMeta):
    def __init__(self):
        self.pizzas = [Pizza(*i) for i in pizza_item]
        self.personal = [Person(*i) for i in cashiers]
        self.receipts = []
    def print_reciepts(self, receipts):
        for i in receipts:
            print(i)


    @decor
    def print_menu(self, pizzas):
        print(f'{"The menu of pizzeria Regenbogen:":^64}')
        for i in pizzas:
            print('-'* 64, f'Name - {i.name}, price - {i.price}',
                  f'description : {i.description}', sep='\n')

    def hello(self):
        print("Welcome to pizzeria Regenbogen!", "If you would view our menu - enter 1", "If you want to exit - enter 0",
"Pizzas that the price is less 175 - enter 2", "Pizzas that the price is more 175 - enter 3",
"To see the cashiers - enter 4", sep="\n")

    def cashier(self):
        staff = itertools.cycle(el.surname for el in self.personal)
        for x in range(random.randint(5, 20)):
            a = random.sample(self.pizzas, random.randint(1, 11))
            staff1 = next(staff)
            print('-' * 64, f'Cashier {staff1}. Pizzas: {[el.name for el in a]}', sep='\n')
        print('-' * 64)