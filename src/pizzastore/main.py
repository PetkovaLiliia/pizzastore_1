
from my_class.PizzaStore import PizzaStore
from constants.cashiers import cashiers

regenbogen = PizzaStore()

while True:
    regenbogen.hello()
    a = input("Please, enter the number: ")
    match a:
        case "0":
            print("Good luck!")
            break
        case "1":
            regenbogen.print_menu(regenbogen.pizzas)
        case "2":
            lis = [el for el in regenbogen.pizzas if el.price < 175]
            regenbogen.print_menu(lis)
        case "3":
            lis = [el for el in regenbogen.pizzas if el.price > 175]
            regenbogen.print_menu(lis)

        case "4":
            regenbogen.cashier()
        case _:
            print("Do not worry, you enter wrong input. Please, try again!")


