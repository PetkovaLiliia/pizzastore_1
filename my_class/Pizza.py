class Pizza:
    def __init__(self, idx, na, price, des):
        self.idx = idx
        self.name = na
        self.price = price
        self.description = des
    def __str__(self):
        return f'Pizza {self.name}, price - {self.price}. {self.description}'

    def print_in_receipt(self):
        return f'Pizza {self.name}'

