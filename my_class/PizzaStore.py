import csv
from pizzastore.my_class.Receipt import Receipt
from pizzastore.my_class.pizza import Pizza

read_pizza = open('src\pizzastore\constants\pizza1', "r")
pizza_items = csv.reader(read_pizza, delimiter=",", quoting=csv.QUOTE_MINIMAL)


def decor(method):
    def dec(s, x):
        print('*' * 64)
        method(s, x)
        print('-'*64, f"Numbers of orders: {len(x)}", sep='\n')
        print('*' * 64)
    return dec

class SingletonMeta(type):

    _instances = {}

    def __call__(cls, *args, **kwargs):

        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]

class PizzaStore(metaclass=SingletonMeta):
    def __init__(self):
        self.pizzas = [Pizza(int(i[0]),i[1],int(i[2]),i[3]) for i in pizza_items]

        self.receipts = []


    def print_receipts(self):
        print('*' * 64, self.receipts[-1], '*' * 64, sep='\n')


    def add_receipt(self, r_item: Receipt):
        self.receipts.append(r_item)

    def find_line(self, name):
        for li in self.receipts:
            if name in li:
                print(self.receipts[li])


    @decor
    def print_menu(self, pizzas):
        print(f'{"The menu of pizzeria Regenbogen:":^64}')
        for i in pizzas:
            print('-'*64, f'Name - {i.name}, price - {i.price}',
                  f'description : {i.description}', sep='\n')

    def hello(self):
        print("Welcome to pizzeria Regenbogen!", "If you would view our menu - enter 1", "If you want to exit - enter 0",
"Pizzas that are cheaper than the listed price - enter 2", "generate a random receipt - enter 3",
"To see the cashiers - enter 4", sep="\n")

