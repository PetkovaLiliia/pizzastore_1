from pizzastore.my_class.pizza import Pizza

class ReceiptLine:
    def __init__(self, pizza: Pizza, num=1):
        self.pizza = pizza
        self.num = num

    def __str__(self):
        return f'{self.pizza.print_in_receipt():<42}{self.num}*{self.pizza.price:<12}|{self.num*self.pizza.price:^7}'


    def change_num(self, amount:int):
        if amount > 0:
            self.num = amount
        if amount < 0:
            print("Please, enter the correct amount")

