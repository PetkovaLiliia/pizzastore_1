from datetime import datetime

from pizzastore.my_class.Person import Person
from pizzastore.my_class.ReceiptLine import ReceiptLine
import itertools
import csv

staff_1 = open("src\pizzastore\constants\cashiers", "r")
cashier = csv.reader(staff_1, delimiter=",", quoting=csv.QUOTE_MINIMAL)

staff = itertools.cycle([Person(*i) for i in cashier])
def get():
    for item_cashier in staff:
        return item_cashier

class Receipt:
    n_Receipt = 1
    def __init__(self):
        self.data = datetime.today().strftime("%d.%m.%Y  %H:%M")
        self.lines = []
        self.num = Receipt.n_Receipt
        Receipt.n_Receipt += 1
        self.ind = 0

    def add_line(self, line: ReceiptLine):
        self.lines.append(line)


    def __str__(self):
        a = "\n".join([str(item) for item in self.lines])
        b = '-'*64
        return f'{get()} {self.data}, check number {self.num} \n{b} \n{a}'


