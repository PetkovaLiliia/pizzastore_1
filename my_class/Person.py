
class Person:
    def __init__(self, cashier, name, surname):
        self.cashier = cashier
        self.name = name
        self.surname = surname


    def print_person(self):
        return f'{self.surname}'

    def __str__(self):
        return f'{self.name} {self.surname}.'
