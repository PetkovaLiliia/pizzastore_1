
from my_class.PizzaStore import PizzaStore
import random
from my_class.Receipt import Receipt
from my_class.ReceiptLine import ReceiptLine
from pizzastore.my_class.exception import MyException

regenbogen = PizzaStore()

while True:
    regenbogen.hello()
    a = input("Please, enter the number: ")
    match a:
        case "0":
            print("Good luck!")
            break
        case "1":
            regenbogen.print_menu(regenbogen.pizzas)
        case "2":
            try:
                su = int(input('Enter price: '))
                if su <= 0:
                    raise MyException("interessant")
                lis = [el for el in regenbogen.pizzas if el.price < su]
                regenbogen.print_menu(lis)

            except MyException:
                print('*'*64, "Please, enter the correct amount", '*'*64, sep='\n ')

        case "3":
            check = Receipt()
            a = random.sample(regenbogen.pizzas, random.randint(1, 11))
            for i in a:
                check.add_line(ReceiptLine(i, random.randint(1, 5)))
            regenbogen.add_receipt(check)
            regenbogen.print_receipts()

        case "4":
            n = int(input("The number of check: "))
            check = regenbogen.receipts[n-1]
            pizza = input("What pizza do you want to change: ")
            for line in check.lines:
                if pizza == line.pizza.name:
                    amount = int(input("num: "))
                    line.change_num(amount)
                    print('*'*64, check, '*'*64, sep='\n')
                    break
                else:
                    print("There is no such pizza in your check!")
        case _:
            print("Do not worry, you enter wrong input. Please, try again!")
